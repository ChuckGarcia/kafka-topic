package mx.demo.kafka.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public final class MyKafkaListener {

    @KafkaListener(topics = "${topic.name.consumer}" , groupId = "${spring.kafka.consumer.group-id}",concurrency = "2")
    public void messageListener(final String message){
        System.out.printf("Receiving message: {%s}%n", message);
        this.processMessage(message);
    }

    private void processMessage(final String message) {
        System.out.println(message);
    }

}
