# Kafka topic with Java & Springboot

This is an example of how to create a Kafka topic using Java and Springboot. This example creates a simple producer to send a message to a topic, it also creates 2 consumers suscribed to the topic.


## Documentation

More information about kafka can be found in
[official documentation](https://kafka.apache.org/documentation/)



## Installation of Apache Kafka on Windows
To run this project you must install Apache Kafka server. This example shows how to install and run Apache Kafka on Windows.

NOTE: Your local environment must have Java 8+ installed.

#### 1. Download Kafka
[Download](https://www.apache.org/dyn/closer.cgi?path=/kafka/3.5.0/kafka_2.13-3.5.0.tgz) apache kafka and extract it into a folder.

For example: C:\kafka

#### 2. Start zookeeper

Open command promt in the kafka folder and start zookeeper using the command given below:

```bash
.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties
```
#### 3. Start kafka

Open a new command promt in the kafka folder and run kafka server using the command given below:

```bash
.\bin\windows\kafka-server-start.bat .\config\server.properties
```
Once all services have successfully launched, you will have a basic Kafka environment running and ready to use.
## Run Locally

Clone the project:

```bash
  git clone https://gitlab.com/ChuckGarcia/kafka-topic.git
```

Go to the project directory:

```bash
  cd kafka-topic
```

Install dependencies:

```bash
  mvn clean install
```

Start the producer:

```bash
  cd kafka-producer
  mvn spring-boot:run
```
NOTE: producer will use 8080 port by default.

Start the consumer:

```bash
  cd kafka-consumer
  mvn spring-boot:run
```
NOTE: consumer will use 8081 port by default.

## API Reference

#### Send a message to the kafka topic

To send a message to kafka topic use the API created in producer module. See the details below:

```http
  GET /send?msg=hola%20mundo
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `msg` | `string` | **Required**. The message to send  |


