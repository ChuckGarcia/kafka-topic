package mx.demo.kafka.controller;

import mx.demo.kafka.producer.MyKafkaProducer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public final class KafkaController {

    private final MyKafkaProducer myKafkaProducer;

    public KafkaController(MyKafkaProducer myKafkaProducer){
        this.myKafkaProducer = myKafkaProducer;
    }

    @GetMapping("/send")
    public void send (@RequestParam final String msg){
        this.myKafkaProducer.send(msg);
    }
}
