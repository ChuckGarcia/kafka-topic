package mx.demo.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public final class MyKafkaProducer {

    private final KafkaTemplate kafkaTemplate;
    @Value("${topic.name.producer}")
    private String topicName;

    @Autowired
    public MyKafkaProducer(final KafkaTemplate kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void send(final String message) {
        System.out.printf("Sending message: {%s}%n", message);
        this.kafkaTemplate.send(this.topicName, message);
    }
}

